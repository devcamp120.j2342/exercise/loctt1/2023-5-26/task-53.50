import models.Account;

public class AccountClass{
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("1", "Loc");
        Account account2 = new Account("2", "Cam", 1000);

        System.out.println("Account 1: ");
        // System.out.println(account1.credit(2000));
        // System.out.println(account1.toString());
        // System.out.println(account1.debit(1000));
        // System.out.println(account1.toString());

        System.out.println(account1.transferTo(account2, 2000));
        System.out.println(account2.toString());



        // System.out.println("--------------------------------");
        // System.out.println("Account 2: ");
        // // System.out.println(account2.credit(3000));
        // // System.out.println(account2.toString());
        // System.out.println(account1.debit(5000));
        // System.out.println(account2.toString());
        System.out.println(account2.transferTo(account1, 2000));
        System.out.println(account1.toString());
    }
}
